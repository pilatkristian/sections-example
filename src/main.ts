import './tailwind.css'

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
  <main>
    <header class="container mt-20 bg-red">
    	<h1 class="text-5xl text-center">Sections</h1>
	</header>
	<section class="section">
		<div class="container">
			<h2 class="section-heading text-3xl">Section without background</h2>
			<article class="grid grid-cols-2 gap-20">
			<div>
				<h3 class="text-2xl">Section</h3>
				<p>Basic section with top and bottom padding.</p>
			</div>
			<img src="https://placehold.co/700x500" alt="image">
			</article>
		</div>
	</section>
	<section class="section">
		<div class="container">
			<h2 class="section-heading text-3xl">Section without background</h2>
			<article class="grid grid-cols-2 gap-20">
			<img src="https://placehold.co/700x500" alt="image">
			<div>
				<h3 class="text-2xl">Shifted up</h3>
				<p>Two consecutive sections without background would be devided by double padding. That's why this section needs to be shifted up with help of negative top margin.</p>
			</div>
			</article>
		</div>
	</section>
	<section class="section bg-fuchsia-100">
		<div class="container">
			<h2 class="section-heading text-3xl">Section with background</h2>
			<article class="grid grid-cols-2 gap-20">
			<div>
				<h3 class="text-2xl">Double padding</h3>
				<p>No need to shift up the section here.</p>
			</div>
			<img src="https://placehold.co/700x500" alt="image">
			</article>
		</div>
	</section>
	<section class="section bg-fuchsia-100">
		<div class="container">
			<h2 class="section-heading text-3xl">Section with background</h2>
			<article class="grid grid-cols-2 gap-20">
			<img src="https://placehold.co/700x500" alt="image">
			<div>
				<h3 class="text-2xl">Shifted up</h3>
				<p>Once again two consecutive sections with the same background have to be shifted up.</p>
			</div>
			</article>
		</div>
	</section>
	<section class="section" data-component="specific-component">
		<div class="container">
			<h2 class="section-heading text-3xl">Section with specific component</h2>
			<article class="grid grid-cols-2 gap-20">
			<div>
				<h3 class="text-2xl">Different spacing</h3>
				<p>This section has "data-component" attribute. Check out tailwind.css line 30.</p>
			</div>
			<img src="https://placehold.co/700x500" alt="image">
			</article>
		</div>
	</section>
	<section class="section" data-component="specific-component">
		<div class="container">
			<h2 class="section-heading text-3xl">Section with specific component</h2>
			<article class="grid grid-cols-2 gap-20">
			<img src="https://placehold.co/700x500" alt="image">
			<div>
				<h3 class="text-2xl">Different spacing</h3>
				<p>This section has "data-component" attribute. Check out tailwind.css line 30.</p>
			</div>
			</article>
		</div>
	</section>
  </main>
`